package ch05;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

class Student2 {
    private String name;
    private String number;

    Student2(String name, String number) {
        this.name = name;
        this.number = number;
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, number);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Student2 other = (Student2) obj;
        if (!Objects.equals(this.name, other.name)) {
            return false;
        }
        return Objects.equals(this.number, other.number);
    }

    @Override
    public String toString() {
        return String.format("(%s,%s)", name, number);
    }
}

public class Students2 {
    public static void main(String[] args) {
        Set students = new HashSet();
        students.add(new Student2("Queen", "B624920"));
        students.add(new Student2("king", "B624921"));
        students.add(new Student2("Alan", "B624922"));
        System.out.println(students);
    }
}
