package ch05;

class FinallyTest{
    public static void main(String args[]) {
        try{
            int x=0;
            int y=20;
            int z=y/x;
            System.out.println("y/x 的值是 :"+z);
        }catch(ArithmeticException e){
            System.out.println(" 捕获到算术异常 ： "+e);
        }
        finally{
            System.out.println(" 执行到 finally 块内！");
            try{
                String name=null;
                if(name.equals(" LXM ")){
                    System.out.println(" 你的名字叫XYH！");
                }
            }
            catch(Exception e){
                System.out.println(" 又捕获到另一个异常 ： "+e);
            }
            finally{
                System.out.println(" 执行到内层的 finally 块内！");
            }
        }
    }
}
