package ch05;

/**
 * Created by Cai Ye on 2016/3/29.
 */

import java.util.HashSet;
import java.util.Set;

class Student {
    private String name;
    private String number;

    Student(String name, String number) {
        this.name = name;
        this.number = number;
    }

    @Override
    public String toString() {
        return String.format("(%s,%s)", name, number);
    }
}

public class Students {
    public static void main(String[] args) {
        Set students = new HashSet();
        students.add(new Student("Queen", "B624920"));
        students.add(new Student("king", "B624921"));
        students.add(new Student("Alan", "B624922"));
        System.out.println(students);
    }
}
