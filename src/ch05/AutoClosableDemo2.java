package ch05;

import static java.lang.System.out;

public class AutoClosableDemo2 {
    public static void main(String[] args) {
        try (ResourceSome some = new ResourceSome();
             ResourceOther other = new ResourceOther()) {
            some.doSome();
            other.doOther();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}

class ResourceSome implements AutoCloseable {
    void doSome() {
        out.println("do some thing!");
    }

    @Override
    public void close() throws Exception {
        out.println("资源 Some 被关闭");
    }
}

class ResourceOther implements AutoCloseable {
    void doOther() {
        out.println("do other thing");
    }

    @Override
    public void close() throws Exception {
        out.println("资源 Other 被关闭");
    }
}