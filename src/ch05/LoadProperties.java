package ch05;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
public class LoadProperties {
    public static void main(String[] args) throws IOException {
        Properties props = new Properties();
        props.load(new FileInputStream("F:\\学习\\java\\person.properties"));
        System.out.println(props.getProperty("ch05.username"));
        System.out.println(props.getProperty("ch05.password"));
    }
}
