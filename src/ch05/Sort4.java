package ch05;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class Sort4 {
    public static void main(String[] args) {
        List words = Arrays.asList("Q", "A", "Z", "W", "S", "X", "E");
        Collections.sort(words);
        System.out.println(words);
    }
}
