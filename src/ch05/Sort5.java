package ch05;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

class StringComparator implements Comparator<String> {
    @Override
    public int compare(String s1, String s2) {
        return -s1.compareTo(s2);
    }
}

public class Sort5 {
    public static void main(String[] args) {
        List<String> words = Arrays.asList("Q", "A", "Z", "W", "S", "X", "E");
        Collections.sort(words, new StringComparator());
        System.out.println(words);
    }
}
