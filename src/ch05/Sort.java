package ch05;


import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class Sort {
    public static void main(String[] args) {
        List numbers = Arrays.asList(9, 1, 7, 3, 6, 13, 17);
        Collections.sort(numbers);
        System.out.println(numbers);
    }
}
