package ch05;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

class Account {
    private String name;
    private String number;
    private int balance;

    Account(String name, String number, int balance) {
        this.name = name;
        this.number = number;
        this.balance = balance;
    }

    @Override
    public String toString() {
        return String.format("Account(%s,%s,%d)", name, number, balance);
    }
}

public class Sort2 {
    public static void main(String[] args) {
        List accounts = Arrays.asList(
                new Account("Queen", "X1221", 800),
                new Account("King", "X3231", 200),
                new Account("Alan", "X6776", 500)
        );
        Collections.sort(accounts);
        System.out.println(accounts);
    }
}
