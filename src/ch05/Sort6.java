package ch05;

import java.util.Arrays;
import java.util.List;

import static java.util.Comparator.nullsFirst;
import static java.util.Comparator.reverseOrder;

public class Sort6 {
    public static void main(String[] args) {
        List words = Arrays.asList("Q", "A", "Z", "W", null, "S", "X", "E", null);
        words.sort(nullsFirst(reverseOrder()));
        System.out.println(words);
    }
}
