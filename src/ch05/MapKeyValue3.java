package ch05;

import java.util.Map;
import java.util.TreeMap;

public class MapKeyValue3 {
    public static void main(String[] args) {
        Map map = new TreeMap<>();
        map.put("one", "一");
        map.put("two", "二");
        map.put("three", "三");
        map.forEach((key, value) -> System.out.printf("(键 %s, 值 %s)%n", key, value)
        );
    }
}
