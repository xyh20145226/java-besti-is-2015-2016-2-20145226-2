package ch05;

public class AutoClosableDemo {
    public static void main(String[] args) {
        try (Resource res = new Resource()) {
            res.doSome();
        } catch (Exception ex) {
            ex.printStackTrace();
            return;
        } finally {
            System.out.println("finally…");
        }
    }
}

class Resource implements AutoCloseable {
    void doSome() {
        System.out.println("do some thing");
    }

    @Override
    public void close() throws Exception {
        System.out.println("资源被关闭");
    }
}