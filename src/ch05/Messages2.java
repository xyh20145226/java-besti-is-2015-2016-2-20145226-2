package ch05;

import java.util.Map;
import java.util.TreeMap;

public class Messages2 {
    public static void main(String[] args) {
        Map<String, String> messages = new TreeMap<>();
        messages.put("Queen", "Hello!Queen的信息！");
        messages.put("King", "给King的悄悄话");
        messages.put("Alan", "Alan的可爱猫喵喵叫");
        System.out.println(messages);
    }
}
