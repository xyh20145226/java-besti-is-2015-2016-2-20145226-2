package ch05;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

class Account2 implements Comparable<Account2> {
    private String name;
    private String number;
    private int balance;

    Account2(String name, String number, int balance) {
        this.name = name;
        this.number = number;
        this.balance = balance;
    }

    @Override
    public String toString() {
        return String.format("Account2(%s,%s,%d)", name, number, balance);
    }

    @Override
    public int compareTo(Account2 other) {
        return this.balance - other.balance;
    }
}

public class Sort3 {
    public static void main(String[] args) {
        List accounts = Arrays.asList(
                new Account2("Queen", "X1221", 800),
                new Account2("King", "X3231", 200),
                new Account2("Alan", "X6776", 500)
        );
        Collections.sort(accounts);
        System.out.println(accounts);
    }
}
