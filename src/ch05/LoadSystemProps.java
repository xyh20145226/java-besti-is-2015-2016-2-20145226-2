package ch05;


import java.util.Properties;

public class LoadSystemProps {
    public static void main(String[] args) {
        Properties props = System.getProperties();
        System.out.println(props.getProperty("CH5.username"));
        System.out.println(props.getProperty("CH5.password"));
    }
}
