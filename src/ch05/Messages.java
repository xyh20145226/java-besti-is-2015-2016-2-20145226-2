package ch05;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

import static java.lang.System.out;

public class Messages {
    public static void main(String[] args) {
        Map<String, String> messages = new HashMap<>();
        messages.put("Queen", "Hello!Queen的信息！");
        messages.put("King", "给King的悄悄话");
        messages.put("Alan", "Alan的可爱猫喵喵叫");
        Scanner console = new Scanner(System.in);
        out.print("要谁的信息：");
        String message = messages.get(console.nextLine());
        out.println(message);
        out.println(messages);
    }
}
