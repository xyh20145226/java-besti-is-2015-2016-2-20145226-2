package ch05;

import java.util.ArrayDeque;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

public class ForEach {
    public static void main(String[] args) {
        List names = Arrays.asList("Queen", "King", "Alan");
        forEach(names);
        forEach(new HashSet(names));
        forEach(new ArrayDeque(names));
    }

    static void forEach(Iterable iterable) {
        for (Object o : iterable) {
            System.out.println(o);
        }
    }
}
