/**
 * Created by XiaYihua on 2017/5/24.
 */
import java.util.Scanner;

public class ConsoleGame extends GuessGame {
    private Scanner console = new Scanner(System.in);

    @Override
    public void print(String text) {
        System.out.print(text);

    }

    @Override
    public int nextInt() {
        return console.nextInt();
    }
}