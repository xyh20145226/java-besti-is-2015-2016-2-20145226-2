import java.util.*;

public class MySort {
    public static void main(String [] args) {
        String [] toSort = {"aaa:10:1:1",
                "ccc:30:3:4",
                "bbb:50:4:5",
                "ddd:20:5:3",
                "eee:40:2:20"};

        System.out.println("Before sort:");
        for (String str: toSort)
            System.out.println(str);

        HashMap<Integer , String> map = new HashMap<Integer , String>();
        for(String str: toSort){
            map.put(Integer.parseInt(str.split(":")[2]),str);
        }
        TreeMap tm = new TreeMap(map);
        System.out.println("After sort:");
        Iterator<Integer> iter = tm.keySet().iterator();
        while (iter.hasNext()){
            System.out.println(tm.get(iter.next()));
        }
    }
}
