import java.lang.Integer;
/**
 * Created by XiaYihua on 2017/5/3.
 * 20145226 
 */
public class Calc {
    public static void main(String[] args) {

        int result = 0;
        if (args.length != 3) {
            System.out.println("Usage: java Calc operator1 operand(+ - * / %) operator2");
        }
        //+ - x / 和%运算
        else {
            switch (args[1]) {
                case "+":
                    result = plus(Integer.parseInt(args[0]), Integer.parseInt(args[2]));
                    break;
                case "-":
                    result = minus(Integer.parseInt(args[0]), Integer.parseInt(args[2]));
                    break;
                case "*":
                    result = times(Integer.parseInt(args[0]), Integer.parseInt(args[2]));
                    break;
                case "/":
                    result = divide(Integer.parseInt(args[0]), Integer.parseInt(args[2]));
                    break;
                case "%":
                    result = mod(Integer.parseInt(args[0]), Integer.parseInt(args[2]));
                    break;
                default: {
                    System.out.println("输入非法！");
                }
            }
        }
        System.out.println(args[0] + " " + args[1] + " " + args[2] + " = " + result);
    }
    public static int plus(int x, int y) {
        return x + y;
    }
    public static int minus(int x, int y) {
        return x - y;
    }
    public static int times(int x, int y) {
        return x * y;
    }
    public static int divide(int x, int y) {
        return x / y;
    }
    public static int mod(int x, int y) {
        return x % y;
    }
}
