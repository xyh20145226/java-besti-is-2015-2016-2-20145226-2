/**
 * Created by XiaYihua on 2017/1/25.
 */
package ch04;
import static java.lang.System.out;
public class Game2{
    public static void main(String[] args){
        play(Action2.RIGHT);
        play(Action2.UP);
    }
    public static void play(Action2 action){
        switch(action){
            case STOP:
                out.println("播放停止动画");
                break;
            case RIGHT:
                out.println("播放向右动画");
                break;
            case LEFT:
                out.println("播放向左动画");
                break;
            case UP:
                out.println("播放向上动画");
                break;
            case DOWN:
                out.println("播放向下动画");
                break;
        }
    }
}