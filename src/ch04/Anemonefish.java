package ch04;
/**
 * Created by XiaYihua on 2017/1/25.
 */
public class Anemonefish extends Fish
{
    public Anemonefish(String name)
    {
        super(name);
    }
    @Override
    public void swim()
    {
        System.out.printf("小丑鱼 %s 游泳%n",name);
    }
}
