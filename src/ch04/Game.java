package ch04;
/**
 * Created by XiaYihua on 2017/1/25.
 */
import static java.lang.System.out;
public class Game{
    public static void main(String[] args){
        play(Action.RIGHT);
        play(Action.UP);
    }
    public static void play(int action){
        switch(action){
            case Action.STOP:
                out.println("bofangtingzhidonghua");
                break;
            case Action.RIGHT:
                out.println("bofangxiangyoudonghua");
                break;
            case Action.LEFT:
                out.println("bofangxiangzuodonghua");
                break;
            case Action.UP:
                out.println("bofangxiangshangdonghua");
                break;
            case Action.DOWN:
                out.println("bofangxiangxiadonghua");
                break;
            default:
                out.println("buzhichicidongzuo");
        }
    }
}