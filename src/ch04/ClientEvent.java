package ch04;
/**
 * Created by XiaYihua on 2017/1/25.
 */
public class ClientEvent{
    private Client client;
    public ClientEvent(Client client){
        this.client=client;
    }
    public String getName(){
        return client.name;
    }
    public String getIp(){
        return client.ip;
    }
}