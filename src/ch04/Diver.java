package ch04;
/**
 * Created by XiaYihua on 2017/1/25.
 */
public interface Diver extends Swimmer
{
    public abstract void dive();
}
