package ch04;
/**
 * Created by XiaYihua on 2017/1/25.
 */
public interface ClientListener{
    void clientAdded(ClientEvent event);
    void clientRemoved(ClientEvent event);
}