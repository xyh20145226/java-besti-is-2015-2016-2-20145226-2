package ch04;
/**
 * Created by XiaYihua on 2017/1/25.
 */
public interface Flyer
{
    public abstract void fly();
}
