package exam20170322;

/**
 * Created by XiaYihua on 2017/3/22.
 */
public class Student {
    private String name;
    private int ID;
    private int javaGrade;



    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Student other = (Student) obj;
        if (ID != other.ID)
            return false;
        if (javaGrade != other.javaGrade)
            return false;
        if (name == null) {
            if (other.name != null)
                return false;
        } else if (!name.equals(other.name))
            return false;
        return true;
    }
}
