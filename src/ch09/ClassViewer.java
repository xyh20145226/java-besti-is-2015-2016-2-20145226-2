package ch09;

import java.lang.reflect.Constructor;
import java.lang.reflect.Modifier;

import com.mysql.jdbc.Field;

public class ClassViewer {
	public static void main(String[] args) {
		try{
			ClassViewer.view(args[0]);
		}catch (ArrayIndexOutOfBoundsException e) {
			System.out.println("没有指定类");
		}catch (ClassNotFoundException e) {
			System.out.println("没有指定类");
		}
	}

	public static void view(String clzname) throws ClassNotFoundException {
		Class clz = Class.forName(clzname);
		
		showPackageInfo(clz);
		showClassInfo(clz);
		
		System.out.println("{");
		
		showFieldInfo(clz);
		showConstructors(clz);
		showMethodsInfo(clz);
		
		System.out.println("}");
	}

	private static void showMethodsInfo(Class clz) {
		// TODO Auto-generated method stub
		
	}

	private static void showConstructors(Class clz) throws SecurityException {
		Constructor[] constructors = clz.getDeclaredConstructors();
		for(Constructor){
			
		}
		
	}

	private static void showFieldInfo(Class clz) {
		java.lang.reflect.Field[] fields = clz.getDeclaredFields();
		for(java.lang.reflect.Field field:fields){
			System.out.printf("%s %s %s",
					Modifier.toString(field.getModifiers()),
					field.getType().getName(),
					field.getName());
		}
		
	}

	private static void showClassInfo(Class clz) {
		int modifier = clz.getModifiers();
		System.out.printf("%s %s %s",
				Modifier.toString(modifier),
				Modifier.isInterface(modifier)?"Interface":"class",
						clz.getName());
	}

	private static void showPackageInfo(Class clz) {
		Package p = clz.getPackage();
		System.out.printf("package %s;%n",p.getName());		
	}
}
