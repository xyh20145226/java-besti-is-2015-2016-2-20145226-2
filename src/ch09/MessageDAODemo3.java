package ch09;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Scanner;

public class MessageDAODemo3 {
	public static void main(String[] args) throws IOException, SQLException {
		MessageDAO3 messageDAODemo3 = new MessageDAO3(new SimpleConnectionPoolDataSource());

		//在Window的NetBeans中，其Output窗口的标准输入编码是Big5
		Scanner console = new Scanner(System.in,"Big5");

		while(true){
			System.out.print("(1)显示留言 (2)新增留言");
			switch(Integer.parseInt(console.nextLine())){
			case 1:
				messageDAODemo3.get().forEach(message -> {
					System.out.printf("%d\t%s\t%s%n",
							message.getId(),
							message.getName(),
							message.getEmail(),
							message.getMsg());});
				break;
			case 2:
				System.out.print("姓名：");
				String name = console.nextLine();
				System.out.print("邮件：");
				String email = console.nextLine();
				System.out.print("留言：");
				String msg = console.nextLine();
				messageDAODemo3.add(new Message(name,email,msg));
			}
		}
	}
}
