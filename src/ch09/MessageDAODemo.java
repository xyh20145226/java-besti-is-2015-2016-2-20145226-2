package ch09;

import java.sql.SQLException;
import java.util.Scanner;

public class MessageDAODemo {
	public static void main(String[] args) throws SQLException {
		MessageDAO dao = new MessageDAO(
				"jdbc:mysql://local:3306/demo?"+"useUnicode=true&characterEncoding=UTF8", 
				"root","root");
		
		//在Window的NetBeans中，其Output窗口的标准输入编码是Big5
		Scanner console = new Scanner(System.in,"Big5");
		
		while(true){
			System.out.print("(1)显示留言 (2)新增留言");
			switch(Integer.parseInt(console.nextLine())){
			case 1:
				dao.get().forEach(message -> {
					System.out.printf("%d\t%s\t%s%n",
							message.getId(),
							message.getName(),
							message.getEmail(),
							message.getMsg());});
				break;
			case 2:
				System.out.print("姓名：");
				String name = console.nextLine();
				System.out.print("邮件：");
				String email = console.nextLine();
				System.out.print("留言：");
				String msg = console.nextLine();
				dao.add(new Message(name,email,msg));
			}
		}
	}
}
