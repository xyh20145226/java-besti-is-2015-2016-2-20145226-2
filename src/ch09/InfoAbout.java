package ch09;

public class InfoAbout {
	public static void main(String[] args) throws ClassNotFoundException {
		
		try{
			Class clz = String.class;
			
			System.out.println("类名称：" + clz.getName());
			System.out.println("是否为接口：" + clz.isInterface());
			System.out.println("是否为基本类型：" + clz.isPrimitive());
			System.out.println("是否为数组对象：" + clz.isArray());
			System.out.println("父类名称：" + clz.getSuperclass().getName());
			
		}catch (ArrayIndexOutOfBoundsException e) {
			System.out.println("没有指定类名称");
		}
	}
}
