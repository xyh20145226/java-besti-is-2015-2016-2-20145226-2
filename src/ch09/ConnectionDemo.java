package ch09;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionDemo {
	public static void main(String[] args) throws ClassNotFoundException, SQLException {
		Class.forName("com.mysql.jdbc.Driver"); //加载驱动程序
		String jdbcUrl = "jdbc:mysql://localhost:3306/demo";
		String user = "root";
		String passwd = "root"; //取得Connection对象
		
		try(Connection conn = DriverManager.getConnection(jdbcUrl,user,passwd)){
			System.out.printf("已%s数据库联机%n",conn.isClosed()?"关闭":"开启");
		}
	}
}
