package ch09;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import com.mysql.jdbc.DatabaseMetaData;

public class TMessageInfo {
	
	private DataSource dataSource;
	
	public TMessageInfo(DataSource dataSource){
		this.dataSource = dataSource;
	}
	
	public List<ColumnInfo> getAllColumnInfo() {
		List<ColumnInfo> infos = null;
		try(Connection conn = dataSource.getConnection()){
			DatabaseMetaData meta = (DatabaseMetaData) conn.getMetaData();
			ResultSet crs = meta.getColumns("demo", null, "t_message", null);
			infos = new ArrayList<>();
			while(crs.next()){
				ColumnInfo info = toColumnInfo(crs);
				infos.add(info);
			}
		}catch (Exception e) {
			throw new RuntimeException();
		}
		return infos;
	}

	private ColumnInfo toColumnInfo(ResultSet crs) throws SQLException {
		ColumnInfo info = new ColumnInfo();
		info.setName(crs.getString("COLUMN_NAME"));
		info.setType(crs.getString("TYPE_NAME"));
		info.setSize(crs.getInt("COLUMN_SIZE"));
		info.setNullable(crs.getBoolean("IS_NULLABLE"));
		info.setDef(crs.getString("COLUMN_DEF"));
		return info;
	}
}
