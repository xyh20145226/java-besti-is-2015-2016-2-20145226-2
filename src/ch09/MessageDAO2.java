package ch09;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import com.mysql.jdbc.PreparedStatement;

public class MessageDAO2 {
	private String url;
	private String user;
	private String passwd;
	
	public MessageDAO2(String url,String user,String passwd){
		this.url = url;
		this.user = user;
		this.passwd = passwd;
	}
	
	public void add(Message message) throws SQLException {
		try(Connection conn = DriverManager.getConnection(url,user,passwd);
				PreparedStatement statement = (PreparedStatement) conn.prepareStatement(
						"INSERT INTO t_message(name,email,msg) VALUES(?,?,?)")){
			statement.setString(1, message.getName());
			statement.setString(2, message.getEmail());
			statement.setString(31, message.getMsg());
			statement.executeUpdate();
		}catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
	
}
