package ch09;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Array;
import java.sql.Blob;
import java.sql.CallableStatement;
import java.sql.Clob;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.NClob;
import java.sql.PreparedStatement;
import java.sql.SQLClientInfoException;
import java.sql.SQLException;
import java.sql.SQLFeatureNotSupportedException;
import java.sql.SQLWarning;
import java.sql.SQLXML;
import java.sql.Savepoint;
import java.sql.Statement;
import java.sql.Struct;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.Executor;
import java.util.logging.Logger;

import javax.sql.DataSource;

public class SimpleConnectionPoolDataSource implements DataSource{
	
	private Properties props;
	private String url;
	private String user;
	private String passwd;
	private int max;
	private List<Connection> conns;
	
	public SimpleConnectionPoolDataSource() throws IOException{
		this("jdbc.properties");
	}

	public SimpleConnectionPoolDataSource(String configFile) throws IOException {
		props = new Properties();
		props.load(new FileInputStream(configFile));
		
		url = props.getProperty("cc.openhome.url");
		user = props.getProperty("cc.openhome.user");
		passwd = props.getProperty("cc.openhome.passwd");
		max = Integer.parseInt(props.getProperty("cc.openhome.poolmax"));
		
		conns = Collections.synchronizedList(new ArrayList<Connection>());
	}
	
	public synchronized Connection getConnection() throws SQLException {
		if(conns.isEmpty()){
			return new ConnectionWrapper(
					DriverManager.getConnection(url,user,passwd), 
					conns, 
					max
					);
		}else{
			return (Connection) conns.remove(conns.size()-1);
		}
	}

	@Override
	public PrintWriter getLogWriter() throws SQLException {
		return null;
	}

	@Override
	public int getLoginTimeout() throws SQLException {
		return 0;
	}

	@Override
	public Logger getParentLogger() throws SQLFeatureNotSupportedException {
		return null;
	}

	@Override
	public void setLogWriter(PrintWriter arg0) throws SQLException {
	}

	@Override
	public void setLoginTimeout(int arg0) throws SQLException {
	}

	@Override
	public boolean isWrapperFor(Class<?> arg0) throws SQLException {
		return false;
	}

	@Override
	public <T> T unwrap(Class<T> arg0) throws SQLException {
		return null;
	}

	@Override
	public Connection getConnection(String arg0, String arg1) throws SQLException {
		return null;
	}

}

class ConnectionWrapper implements Connection{

	private Connection conn;
	private List<Connection> conns;
	private int max;
	
	public ConnectionWrapper(Connection conn,List<Connection> conns,int max){
		this.conn = conn;
		this.conns = conns;
		this.max = max;
	}
	
	@Override
	public void close() throws SQLException{
		if(conns.size() == max){
			conn.close();
		}else{
			conns.add(this);
		}
	}

	@Override
	public Statement createStatement() throws SQLException {
		return conn.createStatement();
	}
	
	@Override
	public boolean isWrapperFor(Class<?> arg0) throws SQLException {
		return false;
	}

	@Override
	public <T> T unwrap(Class<T> arg0) throws SQLException {
		return null;
	}

	@Override
	public void abort(Executor arg0) throws SQLException {
	}

	@Override
	public void clearWarnings() throws SQLException {
	}

	@Override
	public void commit() throws SQLException {
	}

	@Override
	public Array createArrayOf(String arg0, Object[] arg1) throws SQLException {
		return null;
	}

	@Override
	public Blob createBlob() throws SQLException {
		return null;
	}

	@Override
	public Clob createClob() throws SQLException {
		return null;
	}

	@Override
	public NClob createNClob() throws SQLException {
		return null;
	}

	@Override
	public SQLXML createSQLXML() throws SQLException {
		return null;
	}

	@Override
	public Statement createStatement(int arg0, int arg1) throws SQLException {
		return null;
	}

	@Override
	public Statement createStatement(int arg0, int arg1, int arg2) throws SQLException {
		return null;
	}

	@Override
	public Struct createStruct(String arg0, Object[] arg1) throws SQLException {
		return null;
	}

	@Override
	public boolean getAutoCommit() throws SQLException {
		return false;
	}

	@Override
	public String getCatalog() throws SQLException {
		return null;
	}

	@Override
	public Properties getClientInfo() throws SQLException {
		return null;
	}

	@Override
	public String getClientInfo(String arg0) throws SQLException {
		return null;
	}

	@Override
	public int getHoldability() throws SQLException {
		return 0;
	}

	@Override
	public DatabaseMetaData getMetaData() throws SQLException {
		return null;
	}

	@Override
	public int getNetworkTimeout() throws SQLException {
		return 0;
	}

	@Override
	public String getSchema() throws SQLException {
		return null;
	}

	@Override
	public int getTransactionIsolation() throws SQLException {
		return 0;
	}

	@Override
	public Map<String, Class<?>> getTypeMap() throws SQLException {
		return null;
	}

	@Override
	public SQLWarning getWarnings() throws SQLException {
		return null;
	}

	@Override
	public boolean isClosed() throws SQLException {
		return false;
	}

	@Override
	public boolean isReadOnly() throws SQLException {
		return false;
	}

	@Override
	public boolean isValid(int arg0) throws SQLException {
		return false;
	}

	@Override
	public String nativeSQL(String arg0) throws SQLException {
		return null;
	}

	@Override
	public CallableStatement prepareCall(String arg0) throws SQLException {
		return null;
	}

	@Override
	public CallableStatement prepareCall(String arg0, int arg1, int arg2) throws SQLException {
		return null;
	}

	@Override
	public CallableStatement prepareCall(String arg0, int arg1, int arg2, int arg3) throws SQLException {
		return null;
	}

	@Override
	public PreparedStatement prepareStatement(String arg0) throws SQLException {
		return null;
	}

	@Override
	public PreparedStatement prepareStatement(String arg0, int arg1) throws SQLException {
		return null;
	}

	@Override
	public PreparedStatement prepareStatement(String arg0, int[] arg1) throws SQLException {
		return null;
	}

	@Override
	public PreparedStatement prepareStatement(String arg0, String[] arg1) throws SQLException {
		return null;
	}

	@Override
	public PreparedStatement prepareStatement(String arg0, int arg1, int arg2) throws SQLException {
		return null;
	}

	@Override
	public PreparedStatement prepareStatement(String arg0, int arg1, int arg2, int arg3) throws SQLException {
		return null;
	}

	@Override
	public void releaseSavepoint(Savepoint arg0) throws SQLException {
	}

	@Override
	public void rollback() throws SQLException {
	}

	@Override
	public void rollback(Savepoint arg0) throws SQLException {
	}

	@Override
	public void setAutoCommit(boolean arg0) throws SQLException {
	}

	@Override
	public void setCatalog(String arg0) throws SQLException {
	}

	@Override
	public void setClientInfo(Properties arg0) throws SQLClientInfoException {
	}

	@Override
	public void setClientInfo(String arg0, String arg1) throws SQLClientInfoException {
	}

	@Override
	public void setHoldability(int arg0) throws SQLException {
	}

	@Override
	public void setNetworkTimeout(Executor arg0, int arg1) throws SQLException {
	}

	@Override
	public void setReadOnly(boolean arg0) throws SQLException {
	}

	@Override
	public Savepoint setSavepoint() throws SQLException {
		return null;
	}

	@Override
	public Savepoint setSavepoint(String arg0) throws SQLException {
		return null;
	}

	@Override
	public void setSchema(String arg0) throws SQLException {
	}

	@Override
	public void setTransactionIsolation(int arg0) throws SQLException {
	}

	@Override
	public void setTypeMap(Map<String, Class<?>> arg0) throws SQLException {
	}
}
