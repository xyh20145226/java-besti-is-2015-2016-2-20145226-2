package ch09;

import java.io.IOException;
import java.io.OutputStream;
import java.sql.SQLException;

import javax.sql.rowset.RowSetProvider;
import javax.sql.rowset.WebRowSet;

public class TMessageUtil {
	public static void writeXML(OutputStream outputStream) throws SQLException, IOException {
		
		try(WebRowSet rowSet = RowSetProvider.newFactory().createWebRowSet()){
			rowSet.setUrl("jdbc:mysql://localhost:3306/demo");
			rowSet.setUsername("root");
			rowSet.setPassword("root");
			rowSet.setCommand("SELECT * FROM t_message");
			rowSet.execute();
			rowSet.writeXml(outputStream);
		}
	}
	
	public static void main(String[] args) throws Exception {
		TMessageUtil.writeXML(System.out);
	}
}
