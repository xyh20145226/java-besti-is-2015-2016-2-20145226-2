package ch07;

import java.time.LocalDate;
import java.time.Period;
import java.util.Scanner;

public class HowOld2 {
	public static void main(String[] args) throws Exception {
        System.out.print("请输入出生年月日（yyyy-mm-dd）：");
        LocalDate birth = LocalDate.parse(new Scanner(System.in).nextLine());
        LocalDate now =  LocalDate.now();
        Period period = Period.between(birth, now);
        System.out.printf("你活了%d年%d月%d日 %n"+period.getYears(),period.getMonths(),period.getDays());
    }
}
