package ch07;

import java.time.LocalDate;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import static java.lang.System.out;

public class ZoneDateTimeDemo {
	public static void main(String[] args) {
		LocalTime localTime = LocalTime.of(0, 0, 0);
		LocalDate localDate = LocalDate.of(1996, 2, 8);
		ZonedDateTime zoneDateTime = ZonedDateTime.of(
				localDate,localTime,ZoneId.of("Asia/Taipei"));
		
		out.println(zoneDateTime);
		out.println(zoneDateTime.toEpochSecond());
		out.println(zoneDateTime.toInstant().toEpochMilli());
	}
}
