package ch07;

import java.util.Calendar;
import static java.lang.System.out;

public class CalendarUtil {
	public static void main(String[] args) {
		Calendar birth = Calendar.getInstance();
		birth.set(1996, Calendar.MARCH, 11);
		Calendar now = Calendar.getInstance();
		out.printf("������%d%n",yearBetween(birth,now));
		out.printf("������%d%n",dayBetween(birth,now));
	}

	private static Long dayBetween(Calendar birth, Calendar now) {
		Calendar calendar = (Calendar) birth.clone();
		long days = 0;
		while(calendar.before(now)){
			calendar.add(Calendar.DATE, 1);
			days ++;
		}
		return days - 1;
	}

	private static Long yearBetween(Calendar birth, Calendar now) {
		Calendar calendar = (Calendar) birth.clone();
		long years = 0;
		while(calendar.before(now)){
			calendar.add(Calendar.YEAR, 1);
			years ++;
		}
		return years - 1;
	}
}
