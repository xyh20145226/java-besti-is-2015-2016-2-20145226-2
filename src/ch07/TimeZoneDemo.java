package ch07;

import java.util.TimeZone;
import static java.lang.System.out;

public class TimeZoneDemo {
	public static void main(String[] args) {
        TimeZone timeZone = TimeZone.getDefault();
        out.println(timeZone.getDisplayName());
        out.println("\t时区ID：" + timeZone.getID());
        out.println("\t日光节约时数：" + timeZone.getDSTSavings());
        out.println("\tUTC 偏移毫秒数：" + timeZone.getRawOffset());
    }
}
