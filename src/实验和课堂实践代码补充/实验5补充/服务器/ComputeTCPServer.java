package xhy;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.math.BigInteger;
import java.net.ServerSocket;
import java.net.Socket;
import java.security.Key;
import java.security.MessageDigest;
import java.security.interfaces.RSAPrivateKey;
import java.util.Map;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

import xhy5.DHCoder;

public class ComputeTCPServer {
	//乙方公钥  
	private static byte[] publicKey2;  
	//乙方私钥  
	private static byte[] privateKey2;  
	//乙方本地密钥  
	private static byte[] key2;

	public static void main(String srgs[]) {
		ServerSocket sc = null;
		Socket socket=null;
		try {
			sc= new ServerSocket(5288);//创建服务器套接字
			System.out.println("端口号:" + sc.getLocalPort());
			System.out.println("服务器已经启动...");
			socket = sc.accept();   //等待客户端连接
			System.out.println("已经建立连接");
			//获得网络输入流对象的引用
			BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			////获得网络输出流对象的引用
			PrintWriter out=new PrintWriter(new BufferedWriter(new OutputStreamWriter(socket.getOutputStream())),true);

			/*// 使用服务器端RSA的私钥对DES的密钥进行解密
			String line = in.readLine();
			BigInteger cipher = new BigInteger(line);
            FileInputStream f = new FileInputStream("Skey_RSA_priv.dat");
            ObjectInputStream b = new ObjectInputStream(f);
            RSAPrivateKey prk = (RSAPrivateKey) b.readObject();
            BigInteger d = prk.getPrivateExponent();
            BigInteger n = prk.getModulus(); //mod n
            BigInteger m = cipher.modPow(d, n); //m=d (mod n)
            System.out.println("d= " + d);
            System.out.println("n= " + n);
            System.out.println("m= " + m);
            byte[] keykb = m.toByteArray();*/

			//由甲方公钥产生本地密钥对 
			String line = in.readLine();
			Map<String, Object> keyMap2 = DHCoder.initKey(line.getBytes());  
			publicKey2 = DHCoder.getPublicKey(keyMap2);  
			privateKey2 = DHCoder.getPrivateKey(keyMap2);
			key2 = DHCoder.getSecretKey(line.getBytes(), privateKey2);
			out.println(publicKey2);
			

			// 使用DES对密文进行解密
			String readline = in.readLine();//读取客户端传送来的数据
			FileInputStream f2 = new FileInputStream("keykb1.dat");
			int num2 = f2.available();
			byte[] ctext = parseHexStr2Byte(readline);
			Key k = new SecretKeySpec(keykb,"DESede");
			Cipher cp = Cipher.getInstance("DESede");
			cp.init(Cipher.DECRYPT_MODE, k);
			byte[] ptext = cp.doFinal(ctext);
			String p = new String(ptext, "UTF8");//编码转换
			System.out.println("从客户端接收到信息为：" + p); //打印解密结果

			//System.out.println("从客户端接收到信息为：" + line.toString());

			/*// 使用Hash函数检测明文完整性
			String aline3 = in.readLine();
			String x = p;
			MessageDigest m2 = MessageDigest.getInstance("MD5");//使用MD5算法返回实现指定摘要算法的 MessageDigest对象
			m2.update(x.getBytes());
			byte a[] = m2.digest();
			String result = "";
			for (int i = 0; i < a.length; i++) {
				result += Integer.toHexString((0x000000ff & a[i]) | 0xffffff00).substring(6);
			}
			System.out.println(result);
			if (aline3.equals(result)) {
				System.out.println("匹配成功");
			}*/
			/*out.println("匹配成功");*/
			out.println("Result:" + new MyDC().evaluate(p));
			out.close();
			in.close();
			sc.close();
		} catch (Exception e) {
			System.out.println(e);
		}
	}

	private static byte[] parseHexStr2Byte(String readline) {
		if (readline.length() < 1)
			return null;
		byte[] result = new byte[readline.length() / 2];
		for (int i = 0; i < readline.length() / 2; i++) {
			int high = Integer.parseInt(readline.substring(i * 2, i * 2 + 1),16);
			int low = Integer.parseInt(readline.substring(i * 2 + 1, i * 2 + 2),16);
			result[i] = (byte) (high * 16 + low);
		}
		return result;
	}
}
