package xhy5;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.math.BigInteger;
import java.net.Socket;
import java.security.MessageDigest;
import java.security.interfaces.RSAPublicKey;
import java.util.Base64;
import java.util.Map;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;

public class ComputeTCPClient {
	//甲方公钥  
    private static byte[] publicKey1;  
    //甲方私钥  
    private static byte[] privateKey1;  
    //甲方本地密钥  
    private static byte[] key1;  

	public static void main(String srgs[]) {
		try {
			KeyGenerator kg = KeyGenerator.getInstance("DESede");
			kg.init(168);
			SecretKey k = kg.generateKey();
			byte[] ptext2 = k.getEncoded();
			//Socket socket = new Socket("222.28.128.64", 5288);
			Socket socket = new Socket("127.0.0.1", 5288);
			BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			PrintWriter out = new PrintWriter(new BufferedWriter(new OutputStreamWriter(socket.getOutputStream())), true);
			BufferedReader stdin = new BufferedReader(new InputStreamReader(System.in));

			 //生成甲方密钥对  
	        Map<String, Object> keyMap1 = DHCoder.initKey();  
	        publicKey1 = DHCoder.getPublicKey(keyMap1);  
	        privateKey1 = DHCoder.getPrivateKey(keyMap1);
	        out.print(publicKey1);
	        
	        String line = in.readLine();
	        key1 = DHCoder.getSecretKey(line.getBytes(), privateKey1);    
	        
			/*//RSA算法，使用服务器端的公钥对DES的密钥进行加密
			FileInputStream f3 = new FileInputStream("Skey_RSA_pub.dat");
			ObjectInputStream b2 = new ObjectInputStream(f3);
			RSAPublicKey pbk = (RSAPublicKey) b2.readObject();
			BigInteger e = pbk.getPublicExponent();
			BigInteger n = pbk.getModulus();
			BigInteger m = new BigInteger(ptext2);
			BigInteger c = m.modPow(e, n);
			String cs = c.toString();
			out.println(cs); // 通过网络将加密后的秘钥传送到服务器
*/			
	        System.out.print("请输入待发送的数据：");

			//用DES加密明文得到密文
			String s = stdin.readLine(); // 从键盘读入待发送的数据
			String[] r = new MyBC().toSuffixExpression(s.split(""));
			/*System.out.println(String.join("", r));
			out.println(String.join("", r));*/
			s=String.join("", r);
			
			//使用甲方本地密钥对数据加密  
	        byte[] encode1 = DHCoder.encrypt(s.getBytes("UTF8"), key1);
			
			Cipher cp = Cipher.getInstance("DESede");
			cp.init(Cipher.ENCRYPT_MODE, k);
			//byte ptext[] = s.getBytes("UTF8");
			//byte ctext[] = cp.doFinal(ptext);
			byte ctext[] = cp.doFinal(encode1);
			String str = parseByte2HexStr(ctext);
			out.println(str); // 通过网络将密文传送到服务器

			/*// 将客户端明文的Hash值传送给服务器
			String x = s;
			MessageDigest m2 = MessageDigest.getInstance("MD5");
			m2.update(x.getBytes());
			byte a[] = m2.digest();
			String result = "";
			for (int i = 0; i < a.length; i++)
			{
				result += Integer.toHexString((0x000000ff & a[i]) | 0xffffff00).substring(6);
			}
			System.out.println(result);
			out.println(result);//通过网络将明文的Hash函数值传送到服务器
			 */
			str = in.readLine();// 从网络输入流读取结果
			System.out.println("从服务器接收到的结果为：" + str); // 输出服务器返回的结果

			/*s = in.readLine();// 从网络输入流读取结果
			System.out.println("从服务器接收到的结果为：" + s); // 输出服务器返回的结果
			 */		}
		catch (Exception e) {
			System.out.println(e);
		}
		finally{
			//stdin.close();
			//in.close();
			//out.close();
			//socket.close();
		}
	}

	private static String parseByte2HexStr(byte[] ctext) {
		StringBuffer sb = new StringBuffer();
		for (int i = 0; i < ctext.length; i++)
		{
			String hex = Integer.toHexString(ctext[i] & 0xFF);
			if (hex.length() == 1)
			{
				hex = '0' + hex;
			}
			sb.append(hex.toUpperCase());
		}
		return sb.toString();
	}
}



