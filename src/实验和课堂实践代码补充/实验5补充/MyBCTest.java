package xyh01;

import java.util.Scanner;

public class MyBCTest {
	public static void main(String[] args) {  
		int result;
		try
		{
			Scanner in = new Scanner(System.in);
			MyDC evaluator = new MyDC();

			System.out.println ("请输入对应的中缀表达式: ");
			String expressionStr = in.nextLine();
			String[] expressionStrs = expressionStr.split("");
			String[] newExpressionStrs = new MyBC().toSuffixExpression(expressionStrs);
			for (int i = 0; i < newExpressionStrs.length; i++) {  
				System.out.print(newExpressionStrs[i]); 
			}

			result = evaluator.evaluate(String.join("", newExpressionStrs));
			System.out.print("\n"+result+"");

		}
		catch (Exception IOException)
		{
			System.out.println("Input exception reported");
		}

	}
}
