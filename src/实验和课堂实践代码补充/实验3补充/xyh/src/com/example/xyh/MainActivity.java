package com.example.xyh;

import android.support.v7.app.AppCompatActivity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AnalogClock;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

	/*//private Button btn;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        btn = (Button)this.findViewById(R.id.btn);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, ThirdActivity.class);
                intent.putExtra("message", "20145226���ջ�");
                startActivity(intent);
            }
        });

        Toast toast = Toast.makeText(MainActivity.this,"20145226���ջ�", Toast.LENGTH_LONG);
        toast.show();
    }*/

	int counter = 0;
	int[] colors = { Color.BLACK, Color.BLUE, Color.CYAN,
			Color.DKGRAY, Color.GRAY, Color.GREEN, Color.LTGRAY,
			Color.MAGENTA, Color.RED, Color.WHITE, Color.YELLOW };
	AnalogClock ac ;
	TextView tv;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		ac = (AnalogClock) this.findViewById(R.id.clock);
		tv = (TextView) this.findViewById(R.id.tv);
		
		tv.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				changeColor(ac);
			}
		});
		
	}
	
	public void changeColor(View view) {
		if (counter == colors.length) {
			counter = 0;
		}
		view.setBackgroundColor(colors[counter++]);
	}

}