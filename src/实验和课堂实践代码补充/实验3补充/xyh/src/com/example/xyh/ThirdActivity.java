package com.example.xyh;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;
import android.widget.Toast;

public class ThirdActivity extends AppCompatActivity {
    private TextView tv ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_third);

        tv = (TextView)this.findViewById(R.id.tv);
        Bundle b =new Bundle();
        b=ThirdActivity.this.getIntent().getExtras();
        
        tv.setText(b.getString("message"));

        }
}
