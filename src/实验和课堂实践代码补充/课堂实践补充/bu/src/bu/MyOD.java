package bu;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

public class MyOD {
	public static void main(String[] args) {
		FileInputStream fis = null;
		try {
			fis = new FileInputStream("test2.txt");
			dump(fis);
		}catch(Exception e) {
			System.out.println(e);
		}
	}

	public static void dump(FileInputStream fis) throws IOException{
		try (InputStream input = fis) {
			byte[] data = new byte[1024];
			int k = input.read(data);
			System.out.print("0000010: ");
			for (int i = 0; i < k; i = i + 4) {
				System.out.printf(" %02x%02x%02x%02x ", data[i + 3], data[i + 2], data[i + 1], data[i]);
			}

			System.out.print("\n0000020: ");
			for(int i=0;i<k;i++) {
                if(data[i]!=0) {
                    System.out.printf("%c ",data[i]);
                    if ((int) data[i] == 10) {
						System.out.printf(" \\");
						System.out.printf("n  ");
					}
                }
            }
            
		} catch (IOException e) {
			e.printStackTrace();
		}

	}
}
