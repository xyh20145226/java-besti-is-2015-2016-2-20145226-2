package bu;

import junit.framework.TestCase;

import java.util.Arrays;

import org.junit.Test;

public class ArrayStringTest extends TestCase {
	String s = "Pretty boys and girls.";
	int[] i = {2,5,9,1};
	
	@Test
	public void testCharAt() {
		assertEquals('t',s.charAt(3));
		assertEquals(' ',s.charAt(6));
		assertEquals('g',s.charAt(16));
		assertEquals('.',s.charAt(21));
		//assertEquals('e',s.charAt(3));
		//assertEquals(' ',s.charAt(22));
	}
	
	@Test
	public void testSplit() {
		assertEquals("Pretty",s.split(" ")[0]);
		assertEquals("boys",s.split(" ")[1]);
		assertEquals("and",s.split(" ")[2]);
		assertEquals("girls.",s.split(" ")[3]);
		//assertEquals(".",s.split(" ")[3]);
	}
	
	@Test
	public void testSort() {
		Arrays.sort(i);
		assertEquals(1,i[0]); //1
		assertEquals(2,i[1]); //2
		assertEquals(5,i[2]); //3
		assertEquals(9,i[3]); //4
		//assertEquals(5,i[3]); //5
	}
	
	@Test
	public void testBinarySearch() {
		int c = Arrays.binarySearch(i, 5);
		assertEquals(1,c); //1
		assertEquals(2,c); //2
	}
}
