package bu;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class MyCP {
	public static void main(String[] args) throws IOException {
		Scanner scan=new Scanner(System.in);
		String str=scan.nextLine();
		String[]Nstr=str.split(" ");

		if(Nstr[2].equals("-tx")){
			FileReader reader = new FileReader(Nstr[3]);
			FileWriter writer = new FileWriter(Nstr[4]);
			dumptx(reader, writer);
		}

		if(Nstr[2].equals("-xt")){
			FileReader reader = new FileReader(Nstr[3]);
			FileWriter writer = new FileWriter(Nstr[4]);
			dumpxt(reader, writer);
		}
	}

	public static void dumpxt(FileReader reader, FileWriter writer) {
		try (FileReader input = reader; FileWriter output = writer) {
			char[] data = new char[8];
            char []ch = new char[1];

            while((input.read(data)) != -1) {
                String str="";
                input.read();
                for(int i=0;i<8;i++){
                    str+=data[i];
                }
                int ten=Integer.valueOf(str,2);
                ch[0]=(char)ten;
                output.write(ch, 0, 1);
                output.write(" ");
            }
		}catch (IOException e) {
			e.printStackTrace();
		}

	}

	public static void dumptx(FileReader reader, FileWriter writer) {
		try (FileReader input = reader; FileWriter output = writer) {
			char[] data = new char[1];
            while((input.read(data)) != -1) {
                int da=(int)data[0];
                String str1="";
                String str=Integer.toBinaryString(da);
                for(int i=0;i<8;i++){
                    if(i<(8-str.length()))str1+='0';
                }
                str1+=str;
                output.write(str1, 0, 8);
                output.write(" ");
            }
		}catch (IOException e) {
			e.printStackTrace();
		}
	}
}
