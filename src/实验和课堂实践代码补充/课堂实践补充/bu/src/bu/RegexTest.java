package bu;

import junit.framework.TestCase;
import org.junit.Test;

public class RegexTest extends TestCase{

	@Test
	public void testMatches(){
		//元字符
		assertEquals(true, "CQB".matches("\\w.\\w")); //.表示除了换行符之外任意一个字符
		assertEquals(true, "1258".matches("\\d+")); //匹配1个或更多连续的数字
		assertEquals(true, "0792-69831245".matches("0\\d{3}-\\d{8}"));//匹配4位区号+8位电话号码
		assertEquals(true, "quit".matches("\\bq\\w*\\b"));//匹配以字母q开头的字符串
		assertEquals(true, "apple".matches("\\b\\w{5}\\b"));//匹配长度位5个字符的字符串
		assertEquals(true, "75875832532".matches("^\\d{5,12}$"));//^和$代表分别代表行的开始和结束，重复5~12次

		//字符转义
		assertEquals(true, "Q*N".matches("\\w\\*\\w"));//要查找*，需要用\*
		assertEquals(true, "Q\\N".matches("\\w\\\\\\w"));//要查找\，需要用\\
		assertEquals(true, "Q.N".matches("\\w\\.\\w"));//要查找.，需要用\.
		
		//重复
		assertEquals(true, "qi".matches("qi*")); //*：重复零次或更多次
		assertEquals(true, "qiiiiii".matches("qi*"));//*：重复零次或更多次
		assertEquals(true, "QQ20145226".matches("QQ\\d+"));//+：重复一次或更多次
		assertEquals(true, "qqqqq".matches("q{5}"));//{n}：重复n次
		assertEquals(true, "qqq".matches("q{2,}"));//{n,} 重复n次或更多次
		assertEquals(true, "qqqqqqqq".matches("q{6,9}"));//{n,m} 重复n到m次

		//字符类：在方括号里列出想查找的数字，字母或数字，空白字符等
		assertEquals(true, "q".matches("[quit]"));//匹配[]内的字母或符号
		assertEquals(true, "m".matches("[a-z]"));//匹配[ - ]给出的字母或数字范围
		assertEquals(true, "(0792)69831245".matches("\\(?0\\d{3}[) -]?\\d{8}"));//(1个0，3个数字\d{2})或-或空格出现1次或不出现，八个数字\d{8}。

		//分枝条件
		assertEquals(true, "0792-6983124".matches("0\\d{2}-\\d{8}|0\\d{3}-\\d{7}"));//匹配三个数字-八个数字，或者四个数字-七个数字
		assertEquals(true, "079-69831245".matches("\\(?0\\d{2}\\)?[- ]?\\d{8}|0\\d{2}[- ]?\\d{8}|\\(?0\\d{3}\\)?[- ]?\\d{7}|0\\d{3}[- ]?\\d{7}"));
		assertEquals(true, "2".matches("((1|0?)[0-9]|[1-9][0-9])"));

		//分组
		assertEquals(true, "121.353.678.999".matches("(\\d{1,3}\\.){3}\\d{1,3}"));//匹配每三个数字加一个小数点，共12个数字
		assertEquals(true, "219.241.20.3".matches("((2[0-4]\\d|25[0-5]|[01]?\\d\\d?)\\.){3}(2[0-4]\\d|25[0-5]|[01]?\\d\\d?)"));//匹配正确的ip地址

		//反义
		assertEquals(true, "adef".matches("\\S+"));//匹配任意不是空白符的字符
		assertEquals(false, "12aaa".matches("\\D+"));//匹配任意非数字的字符
		assertEquals(false, "xxerey".matches("[^xy]"));//匹配除了[]内字符以外的任意字符
		assertEquals(true, "<quit>".matches("<q[^>]+>"));//匹配用尖括号括起来的以a开头的字符串

		//后向引用
		assertEquals(true, "cheers cheers".matches("\\b(\\w+)\\b\\s+\\1\\b"));//匹配重复的单词
		assertEquals(true, "cheers cheers".matches("\\b(?<bang>\\w+)\\b\\s+\\k<bang>\\b"));//指定组名

		//贪婪与懒惰
		assertEquals(true, "qcbai".matches("q.*i"));//贪婪，匹配最长的以q开始，以i结束的字符串
		assertEquals(true, "qcbai qci qqwai".matches("q.*i"));
		assertEquals(true, "qcbai".matches("q.*?i"));//懒惰，匹配最短的，以q开始，以i结束的字符串
	}
}

