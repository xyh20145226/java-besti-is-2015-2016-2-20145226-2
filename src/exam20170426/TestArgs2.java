/**
 * Created by XiaYihua on 2017/4/26.
 */

public class TestArgs2 {

    public static void main(String[] args) {
        int sum = 0;
        int[] arr = new int[args.length];
        for (int i = 0; i < args.length; i++) {
            arr[i] = Integer.parseInt(args[i]);
        }
        sum = func(arr);
        System.out.println(sum);
    }
    
    public static int func(int[] args) {
        int sum = 0;
        if (args.length == 1) sum += args[0];
        else {
            sum += args[args.length-1];
            args = new int[args.length-1];
        }
        return sum;
    }
