/**
 * Created by XiaYihua on 2017/4/26.
 */

public class TestArgs {
    public static void main(String []args){
        System.out.println("参数个数"+args.length);
        int num;//存储整数类型的参数
        int sum=0;//参数的和
        for(String arg:args)//循环
        {
            num=Integer.parseInt(arg);//将字符串参数强制转换成int型
            sum=sum+num;
        }
        System.out.println("几个数相加之和是"+sum);
    }
}
