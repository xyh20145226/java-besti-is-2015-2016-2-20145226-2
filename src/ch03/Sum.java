package ch03;
import java.util.Scanner;
/**
 * Created by XiaYihua on 2017/1/24.
 */
public class Sum {
    public static void main(String[] args) {
        Scanner scanner=new Scanner(System.in);
        long sum=0;
        long number=0;
        do {
            System.out.print("输入数字：");
            number=Long.parseLong(scanner.nextLine());
            sum +=number;
        }while(number!=0);
        System.out.println("总和："+sum);
    }
}
