package ch03;

import java.util.Arrays;

/**
 * Created by XiaYihua on 2017/1/24.
 */
public class Score2 {
    public static void main(String[] args) {
        int[] scores= new int[10];
        for(int score :scores){
            System.out.printf("%2d",score);
        }
        System.out.println();
        Arrays.fill(scores,60);
        for(int score:scores){
            System.out.printf("%3d",score);
        }
    }
}
