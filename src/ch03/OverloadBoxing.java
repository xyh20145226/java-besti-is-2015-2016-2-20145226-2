package ch03;

/**
 * Created by XiaYihua on 2017/1/24.
 */
class Some{
    void someMethod(int i){
        System.out.println("int 版本被调用");
    }
    void someMethod(Integer integer){
        System.out.println("Integer 版本被调用");
    }
}
public class OverloadBoxing {
    public static void main(String[] args) {
        Some s=new Some();
        s.someMethod(1);
    }
}
