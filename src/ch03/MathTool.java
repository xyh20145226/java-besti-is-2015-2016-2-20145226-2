package ch03;

/**
 * Created by XiaYihua on 2017/1/24.
 */
public class MathTool {
    public static int sum(int... numbers) {
        int sum=0;
        for(int number:numbers){
            sum+=number;
        }
        return sum;
    }
}
