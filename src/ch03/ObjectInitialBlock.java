package ch03;
/**
 * Created by XiaYihua on 2017/1/24.
 */
    class Other{
        {
            System.out.println("对象初始区块：");
        }

        Other(){
            System.out.println("Other() 构造函数");
        }

        Other(int o){
            this();
            System.out.printf("Other(int o) 构造函数");
        }
    }

    public class ObjectInitialBlock{
        public static void main(String[] args) {
            new Other(1);
        }
    }

