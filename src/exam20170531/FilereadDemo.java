/**
 * Created by XiaYihua on 2017/5/31.
 */

import java.io.*;

public class FilereadDemo {
    public static void main(String [] args) throws IOException{
        FileInputStream fis = new FileInputStream(args[3]);
        int i = 0;
        while(true){
            if(i%16==0) System.out.print("0000"+i);
            i++;
            int ch = fis.read();
            if(ch==-1) break;
            System.out.print((char)(ch) + " ");
            if(i%16==0) System.out.println();
        }
        fis.close();
    }
}