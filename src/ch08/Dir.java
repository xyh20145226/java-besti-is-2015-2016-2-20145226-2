package ch08;

import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class Dir {
	public static void main(String[] args) throws IOException {
		try(DirectoryStream<Path> directoryStream 
				= Files.newDirectoryStream(Paths.get(args[0]))){
			List<String> files = new ArrayList<>();
			for(Path path : directoryStream){
				if(Files.isDirectory(path)){
					System.out.printf("[%s]%n",path.getFileName());
				}else {
					files.add(path.getFileName().toString());
				}
			}
			
			files.forEach(System.out::println);
		}
	}
}
