package ch08;

import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.PathMatcher;
import java.nio.file.Paths;

public class Ls2 {
	public static void main(String[] args) throws IOException {
		//默认取得所有文档
		String syntax = args.length == 2 ? args[0] : "glob" ;
		String pattern = args.length == 2 ? args[1] : "*" ;
		System.out.println(syntax + "：" + pattern);
		
		//取得目前工作路径
		Path userPath = Paths.get(System.getProperty("user.dir"));
		PathMatcher matcher = FileSystems.getDefault().getPathMatcher(syntax + "：" + pattern);
		
		try(DirectoryStream<Path> directoryStream = 
				Files.newDirectoryStream(userPath)){
			directoryStream.forEach(path -> {
				Path file = Paths.get(path.getFileName().toString());
				if(matcher.matches(file)){
					System.out.println(file.getFileName());
				}
			});
		}
	}
}
