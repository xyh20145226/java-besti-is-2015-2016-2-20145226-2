package ch08;

import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;

public class LoggerDemo2 {
	public static void main(String[] args) {
		Logger logger = Logger.getLogger(LoggerDemo2.class.getName());
		logger.setLevel(Level.FINE);
		for(Handler handler : logger.getParent().getHandlers()){
			handler.setLevel(Level.FINE);
		}
		
		logger.log(Level.WARNING, "WARNING信息");
		logger.log(Level.INFO, "INFO信息");
		logger.log(Level.CONFIG, "CONFIG信息");
		logger.log(Level.FINE, "FINE信息");
	}
}
