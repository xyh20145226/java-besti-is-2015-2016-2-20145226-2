package ch08;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Regex {
	public static void main(String[] args) {
		Scanner console = new Scanner(System.in);
		try{
			System.out.print("输入规则表达式：");
			String regex = console.nextLine();
			System.out.println("输入要比较的文字：");
			String text = console.nextLine();
			print(match(regex,text));
		}catch (Exception e) {
			System.out.println("规则表达式有误：");
			System.out.println(e.getMessage());
		}
	}

	private static void print(List<String> match) {
		if(match.isEmpty()){
			System.out.println("找不到符合文字");
		}else{
			match.forEach(System.out::println);
		}
	}

	private static List<String> match(String regex, String text) {
		Pattern pattern = Pattern.compile(regex);
		Matcher matcher = pattern.matcher(text);
		List<String> matched = new ArrayList<String>();
		while(matcher.find()){
			matched.add(String.format("从索引 %d 开始到索引 %d 之间找到符合文字\"%s\"%n", 
					matcher.start(),matcher.end(),matcher.group()));
		}
		return matched;
	}
}
