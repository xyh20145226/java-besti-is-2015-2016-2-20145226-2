package ch08;

import java.util.logging.Level;
import java.util.logging.Logger;

public class LoggerDemo {
	public static void main(String[] args) {
		Logger logger = Logger.getLogger(LoggerDemo.class.getName());
		
		logger.log(Level.WARNING, "WARNING信息");
		logger.log(Level.INFO, "INFO信息");
		logger.log(Level.CONFIG, "CONFIG信息");
		logger.log(Level.FINE, "FINE信息");
	}
}
