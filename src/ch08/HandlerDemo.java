package ch08;

import java.io.IOException;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;

public class HandlerDemo {
	public static void main(String[] args) throws IOException {
		Logger logger = Logger.getLogger(HandlerDemo.class.getName());
		logger.setLevel(Level.CONFIG);
		
		FileHandler fileHandler = new FileHandler("%h/config.log");
		fileHandler.setLevel(Level.CONFIG);
		
		logger.addHandler(fileHandler);
		logger.config("Logger ��̬���");
	}
}
