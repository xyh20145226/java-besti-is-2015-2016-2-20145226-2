package ch08;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class DirAll {
	public static void main(String[] args) throws IOException {
		Files.walkFileTree(Paths.get(args[0]), new ConsoleFileVisitor());
	}
}
