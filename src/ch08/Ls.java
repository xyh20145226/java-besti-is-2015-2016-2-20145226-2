package ch08;

import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class Ls {
	public static void main(String[] args) throws IOException {
		//默认取得所有文档
		String glob = args.length == 0 ? "*" : args[0] ;
		
		//取得目前工作路径
		Path userPath = Paths.get(System.getProperty("user.dir"));
		
		try(DirectoryStream<Path> directoryStream = 
				Files.newDirectoryStream(userPath,glob)){
			directoryStream.forEach(path -> System.out.println(path.getFileName()));
		}
	}
}
