package ch06;

import java.io.IOException;
import static java.lang.System.out;

public class MemberDemo {
    public static void main(String[] args) throws IOException {
        Member[] members = {
                new Member("B123", "Queen", 80),
                new Member("B456", "King", 85),
                new Member("B789", "Alan", 90)
        };
        for(Member member : members) {
            member.save();
        }
        out.println(Member.load("Queen"));
        out.println(Member.load("King"));
        out.println(Member.load("Alan"));
    }
}