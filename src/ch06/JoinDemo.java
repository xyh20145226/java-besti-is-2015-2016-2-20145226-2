package ch06;


import static java.lang.System.out;

public class JoinDemo {

    public static void main(String[] args) throws InterruptedException {
        out.println("Main thread start...");

        Thread threadB = new Thread(() -> {
            out.println("Thread B start...");
            for (int i = 0; i < 5; i++) {
                out.println("Thread B 执行...");
            }
            out.println("Thread B end...");
        });

        threadB.start();
        threadB.join(); // Thread B 加入 Main thread 流程

        out.println("Main thread end...");
    }
}