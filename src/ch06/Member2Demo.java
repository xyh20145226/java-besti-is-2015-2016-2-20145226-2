package ch06;

import static java.lang.System.out;

public class Member2Demo {
    public static void main(String[] args) throws Exception {
        Member2[] members = {
                new Member2("B123", "Queen", 90),
                new Member2("B456", "King", 92),
                new Member2("B789", "Alan", 98),
                new Member2("B133","Irene",68),
                new Member2("B466","Qi",75),
                new Member2("B799","Klan",82),
        };
        for(Member2 member : members) {
            member.save();
        }
        out.println(Member2.load("B123"));
        out.println(Member2.load("B456"));
        out.println(Member2.load("B789"));
        out.println(Member2.load("B133"));
        out.println(Member2.load("B466"));
        out.println(Member2.load("B799"));
    }
}