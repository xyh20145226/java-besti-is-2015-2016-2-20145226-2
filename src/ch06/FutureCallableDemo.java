package ch06;

import java.util.concurrent.*;
import static java.lang.System.*;

public class FutureCallableDemo {
    static long fibonacci(long n) {
        if (n <= 1) {
            return n;
        }
        return fibonacci(n - 1) + fibonacci(n - 2);
    }

    public static void main(String[] args) throws Exception {
        FutureTask<Long> the32thFibFuture =
                new FutureTask<>(() -> fibonacci(32));

        out.println("老板，我要第 32 个费式数，待会来拿...");

        new Thread(the32thFibFuture).start();
        while(!the32thFibFuture.isDone()) {
            out.println("忙別的事去...");
        }

        out.printf("第 32 个费式数：%d%n", the32thFibFuture.get());
    }
}